import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import CreateContact from "./components/create-contact.component";
import EditContact from "./components/edit-contact.component";
import ContactsList from "./components/contacts-list.component.js";

function App() {
  return (
<Router>
        <div className="container">
          
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to="/" className="navbar-brand">Contacts List</Link>
            <div className="collpase nav-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                  <Link to="/" className="nav-link">  Contacts</Link>
                </li>
                <li className="navbar-item">
                  <Link to="/create" className="nav-link">Create Contact</Link>
                </li>
              </ul>
            </div>
          </nav>

          <Route path="/" exact component={ContactsList} />
          <Route path="/edit/:id" component={EditContact} />
          <Route path="/create" component={CreateContact} />
        </div>
      </Router>
    );
}

export default App;


