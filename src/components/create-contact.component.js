import React, {Component} from 'react';
import axios from 'axios';

export default class CreateContact extends Component {

    constructor(props) {
        super(props);

        this.onChangeContactLastname = this.onChangeContactLastname.bind(this);
        this.onChangeContactFirstname = this.onChangeContactFirstname.bind(this);

        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            contact_lastname: '',
            contact_firstname: ''
        }
    }

    onChangeContactLastname(e) {
        this.setState({
            contact_lastname: e.target.value
        });
    }

    onChangeContactFirstname(e) {
        this.setState({
            contact_firstname: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(`Form submitted:`);
        console.log(`lastname: ${this.state.contact_lastname}`);
        console.log(`firstname: ${this.state.contact_firstname}`);


        const newContact = {
            lastName: this.state.contact_lastname,
            firstName: this.state.contact_firstname,
        };

        axios.post('http://localhost:8080/contact', newContact)
            .then(res => console.log(res.data));

        this.setState({
            contact_firstname: '',
            contact_lastname: ''
        })
    }

    render() {
        return (
            <div style={{marginTop: 20}}>
                <h3>Create New Contact</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Last name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.contact_lastname}
                                onChange={this.onChangeContactLastname}
                                />
                    </div>
                    <div className="form-group">
                        <label>First name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.contact_firstname}
                                onChange={this.onChangeContactFirstname}
                                />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Contact" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}