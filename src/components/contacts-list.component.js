import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Contact = props => (
    <tr>
        <td>{props.contact.lastName}</td>
        <td>{props.contact.firstName}</td>
        <td>
            <Link to={"/edit/"+props.contact._id}>Edit</Link>
        </td>
    </tr>
)

export default class ContactsList extends Component {
    constructor(props) {
        super(props);
        this.state = {contacts: []};
    }

    componentDidMount() {
        axios.get('http://localhost:8080/contact/')
            .then(response => {
                this.setState({ contacts: response.data });
            })
            .catch(function (error){
                console.log(error);
            })
    }

    contactsList() {
        return this.state.contacts.map(function(currentContact, i){
            return <Contact contact={currentContact} key={i} />;
        })
    }

    render() {
        return (
            <div>
                <h3>Contacts List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Last name</th>
                            <th>First name</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.contactsList() }
                    </tbody>
                </table>
            </div>
        )
    }
}